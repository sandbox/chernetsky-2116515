Module allows deleting nodes of specified types without confirmation screen.

* Install and enable module as usuall
* On the module settings page (admin/config/system/delete_quick) choose
node types, that you want to be able to delete without confirmation
* To node's contextual menus will be added "Quick delete" link
* You can also use in you code path 'node/%nid/delete_quick'
for making quick delete links
