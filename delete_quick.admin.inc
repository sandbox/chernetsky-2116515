<?php

/**
 * @file
 * Administration forms for the Delete quick module.
 */

/**
 * Main module settings form.
 */
function delete_quick_settings() {
  $form = array();
  $form['delete_quick'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['delete_quick']['node_types'] = array(
    '#title' => t('Content types'),
    '#type' => 'fieldset',
    '#description' => t('Enable Quick delete functionality for specified content types.') . '<br/><br />',
  );

  // Building node types list.
  $types = node_type_get_types();
  $options = array();
  foreach ($types as $type) {
    $options[$type->type] = $type->name;
  }

  $form['delete_quick']['node_types']['delete_quick_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('delete_quick_nodetypes', array()),
    '#options' => $options,
  );

  return system_settings_form($form);
}
