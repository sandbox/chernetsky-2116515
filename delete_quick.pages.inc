<?php

/**
 * @file
 * Delete quick path actions.
 */

/**
 * Deletes node of specified content type.
 *
 * @param Object $node
 *   The node object on which the operation is to be performed.
 */
function _delete_quick_node_delete_quick($form, &$form_state, $node) {

  // Check if node is allowed for quick deleting.
  $types = variable_get('delete_quick_nodetypes', array());
  if (isset($types[$node->type]) && !empty($types[$node->type])) {

    // Delete node.
    node_delete($node->nid);
    watchdog('content', 'Deleted @type %title.', array('@type' => $node->type, '%title' => $node->title));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => node_type_get_name($node), '%title' => $node->title)));
  }

  // Do a drupal goto here to preserver the 'destination' parameter.
  drupal_goto();
}
